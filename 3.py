ILENAME = "co2_emission.csv"
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def abrir_archivo():
    temp = open(FILENAME)
    diccionario = {}
    suma = 0
    contador = 0

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic

                suma = suma + float
                contador += 1
    temp.close()
    return diccionario


def promedio (suma, contador):
 promedio = suma /contador
 return promedio 

def main():
    suma,contador = openfile()
    promedio = promedio(suma,contador)
    print(" \n El promedio de toneladas emitidas por todos los paises es{promedio}\n")
FILENAME = "co2_emission.csv"
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def abrir_archivo():
    temp = open(FILENAME)
    diccionario = {}
    suma = 0
    contador = 0

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def registros(data):
    menor = 0
    for key, value in data.items():
        pais = data[key]
        menor_pais = key
        
        for key, value in pais.items():
            if key != "codigo":
                contador += 1
            if key != "codigo":
                suma = suma + float(value)
        promedio = suma /contador 

        if promedio < menor:
            menor = promedio
            menor_cantidad = menor_pais  
            print("\n El pais que ha emitido la menor cantidad de toneladas de co2 en promedio es  {menor_cantidad}\n")  

FILENAME = "co2_emission.csv"
(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def abrir_archivo():
    temp = open(FILENAME)
    diccionario = {}
    suma = 0
    contador = 0

    for contador,linea in enumerate(temp):
        if contador != 0:
            temp_linea = linea.split(",")
            pais = temp_linea[PAIS].strip()
            codigo = temp_linea[CODIGO].strip()
            anio = temp_linea[ANIO].strip()
            co2 = temp_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                        anio: co2}

            if diccionario.get(pais):
                diccionario[pais].update(temp_dic)
            else:
                diccionario[pais] = temp_dic
    temp.close()
    return diccionario
def registros(data):
    mayor = 0
    for key, value in data.items():
        pais = data[key]
        mayor_pais = key
        
        for key, value in pais.items():
            if key != "codigo":
                contador += 1
            if key != "codigo":
                suma = suma + float(value)
        promedio = suma /contador 

        if promedio > mayor:
            mayor = promedio
            mayor_cantidad = mayor_pais  
            print("\n El pais que ha emitido mas toneladas de co2 en promedio es  {mayor_cantidad}\n")  
